﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Security.Policy;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageViewer.Model
{
	public class ImageEntity : MVVMBase.ViewModelBase
	{
		#region Fields
		private Uri _imageUrl;

		private Guid _id;
		#endregion
		#region Properties
		public Uri ImageUrl
		{
			get
			{
				if (_imageUrl == null) _imageUrl = new Uri(string.Empty,UriKind.RelativeOrAbsolute);
				return _imageUrl;
			}
			set
			{
				_imageUrl = value;
                ConvertUriToSource(_imageUrl);
				OnPropertyChanged("ImageUrl");
			}
	    }
        
	    private ImageSource _imageSource;
	    public ImageSource ImageSource
	    {
	        get => _imageSource;
	        set
	        {
	            _imageSource = value;
	            OnPropertyChanged("ImageSource");
	        }
	    }

	    private void ConvertUriToSource(Uri url)
	    {
	        BitmapImage bi3 = new BitmapImage();
	        bi3.BeginInit();
	        bi3.UriSource = url;
	        bi3.EndInit();
	        _imageSource =  bi3;
	    }
        
	    public void BitmapToImageSource(Bitmap bitmap)
	    {
	        using (MemoryStream memory = new MemoryStream())
	        {
	            bitmap.Save(memory, System.Drawing.Imaging.ImageFormat.Bmp);
	            memory.Position = 0;
	            BitmapImage bitmapimage = new BitmapImage();
	            bitmapimage.BeginInit();
	            bitmapimage.StreamSource = memory;
	            bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
	            bitmapimage.EndInit();

	            ImageSource = bitmapimage;
	        }
	    }
        public Guid Id
		{
			get
			{
				return _id;
			}
		}
		#endregion
		#region Methods
		public ImageEntity()
		{
			_id = new Guid();
		}
		#endregion
	}
}
