﻿using System;
using System.Collections.ObjectModel;
using ImageViewer.Model;
using MVVMBase;
using System.Windows.Threading;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageViewer.ViewModel
{
    class ImageListViewModel : ViewModelBase, IImageListViewModel
    {
        #region Fields
        private readonly ObservableCollection<ImageEntity> _imageListCollection;
        private ImageEntity _selectedImageEntity;
        private string _status;
        private readonly Dispatcher _currentDispatcher;
        private int _selectedImageEntityIndex;
        private bool _isCollectionVisible = true;
        #endregion

        #region Properties
        public ObservableCollection<ImageEntity> ImageListCollection
        {
            get { return _imageListCollection; }
        }
        public ImageEntity SelectedImageEntity
        {
            get
            {
                return _selectedImageEntity;
            }
            set
            {
                _selectedImageEntity = value;
                OnPropertyChanged("SelectedImageEntity");
                SelectedImageEntityIndex = ImageListCollection.IndexOf(SelectedImageEntity) + 1;
            }
        }

        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }
        public int ImageCount
        {
            get
            {
                return ImageListCollection.Count;
            }
        }
        public Dispatcher CurrentDispatcher
        {
            get
            {
                return _currentDispatcher;
            }
        }
        public int SelectedImageEntityIndex
        {
            get
            {
                return _selectedImageEntityIndex;
            }
            set
            {
                _selectedImageEntityIndex = value;
                OnPropertyChanged("SelectedImageEntityIndex");
            }
        }


        public bool IsCollectionVisible
        {
            get { return _isCollectionVisible; }
            set
            {
                _isCollectionVisible = value; 
                OnPropertyChanged("IsCollectionVisible");
            }
        }
        #endregion

        #region Constructor
        public ImageListViewModel() : this(new ObservableCollection<ImageEntity>(), Dispatcher.CurrentDispatcher) { }
        public ImageListViewModel(ObservableCollection<ImageEntity> imageRepository, Dispatcher CurrentDispatcher)
        {
            _currentDispatcher = CurrentDispatcher;
            _imageListCollection = imageRepository;
            if (_imageListCollection.Count > 0)
            {
                _selectedImageEntity = _imageListCollection[0];
            }
        }
        #endregion
    }
}
