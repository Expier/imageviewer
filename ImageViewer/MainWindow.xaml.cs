﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ImageViewer.ViewModel;
using System.Collections.ObjectModel;
using ImageViewer.Model;
using System.Diagnostics;
using System.Threading;
using System.ComponentModel;
using System.Windows.Threading;

namespace ImageViewer
{
	/// <summary>
	/// Logique d'interaction pour MainWindow.xaml
	/// </summary>
	/// 
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();


		}

        protected override void OnInitialized(EventArgs e)
		{
			base.OnInitialized(e);
			// Variable declaration
			var StringList = new ObservableCollection<ImageEntity>();
            
		    IImageListViewModel imageListViewModel;
		    IMainWindowViewModel mainWindowViewModel;
            imageListViewModel = new ImageListViewModel(StringList, Dispatcher.CurrentDispatcher);
			mainWindowViewModel = new MainWindowViewModel(imageListViewModel);
			this.DataContext = mainWindowViewModel;
		}

		private void Window_Drop(object sender, DragEventArgs e)
		{
			var w = sender as FrameworkElement;
			var dc = w.DataContext as IMainWindowViewModel;
			if (dc != null)
			{
				if (e.Data.GetDataPresent(DataFormats.FileDrop))
				{
					string[] droppedFilePaths =
					e.Data.GetData(DataFormats.FileDrop, true) as string[];

					foreach (string droppedFilePath in droppedFilePaths)
					{
						dc.DroppedFiles.Add(new ImageEntity { ImageUrl = new Uri(droppedFilePath, UriKind.RelativeOrAbsolute) });
					}
					Commands.ApplicationCommands.ManageDroppedFilesCommand.Execute(dc);
				}
			}
		}

	    private void MainWindow_OnKeyDown(object sender, KeyEventArgs e)
	    {
            var w = sender as FrameworkElement;
            var dc = w.DataContext as IMainWindowViewModel;
            var imageListViewModel = dc.imageListViewModel;
            if (e.Key == Key.Down)
            {
                if (Commands.ApplicationCommands.SelectNextImageCanExecute(imageListViewModel))
                    Commands.ApplicationCommands.SelectNextImageExecute(imageListViewModel);
                e.Handled = true;
            }
            else if (e.Key == Key.Up)
            {
                if (Commands.ApplicationCommands.SelectPreviousImageCanExecute(imageListViewModel))
                    Commands.ApplicationCommands.SelectPreviousImageExecute(imageListViewModel);
                e.Handled = true;
            }
            else if (e.Key == Key.Escape)
            {
                imageListViewModel.IsCollectionVisible = true;
            }
        }
	}
}
