﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using MVVMBase;
using ImageViewer.ViewModel;
using SuperfastBlur;

namespace ImageViewer.Commands
{
    public partial class ApplicationCommands
    {
        #region Fields
        private static ICommand _blurImageCommand;
        #endregion
        #region Properties
        public static ICommand BlurImageCommand
        {
            get
            {
                if (_blurImageCommand == null)
                {
                    _blurImageCommand = new RelayCommand(BlurImageExecute, BlurImageCanExecute);
                }
                return _blurImageCommand;
            }
        }
        #endregion
        #region Methods
        public static bool BlurImageCanExecute(object o)
        {
            if (o != null)
            {
                var vm = o as IImageListViewModel;
                return vm.SelectedImageEntity != null;
            }
            return false;
        }
        public static void BlurImageExecute(object o)
        {
            if (o != null)
            {
                var vm = o as IImageListViewModel;
                //vm.SelectedImageEntity = vm.ImageListCollection[vm.ImageListCollection.IndexOf(vm.SelectedImageEntity) + 1];  

                var blur = new GaussianBlur(vm.SelectedImageEntity.ImageUrl.AbsolutePath).Process(10);
                vm.SelectedImageEntity.BitmapToImageSource(blur);
            }
        }
        #endregion
    }
}
