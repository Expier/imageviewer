﻿using ImageViewer.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageViewer.View
{
    /// <summary>
    /// Interaction logic for ImageCollectionView.xaml
    /// </summary>
    public partial class ImageCollectionView : UserControl
    {
        public ImageCollectionView()
        {
            InitializeComponent();
        }

        private void ImageCollectionView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var listBox = sender as ListBox;
            listBox.ScrollIntoView(listBox.SelectedItem);
        }

        private void ImageListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var w = sender as FrameworkElement;
            var dc = w.DataContext as IImageListViewModel;
            if (dc != null)
            {
                //Commands.ApplicationCommands.OpenImageFileCommand.Execute(dc);
                dc.IsCollectionVisible = !dc.IsCollectionVisible;
            }
        }
    }
}
